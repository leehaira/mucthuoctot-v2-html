//var function init call
var initReady = function(){
  //function run
  sidebarScroll();
  notifySlider();
  mainSlider();
  clientsSlider();
  homeBannerSlider();
  homeProductSlider();
  mapContentScroll();
  mapContentClick();
  homePromotionSlider();
  footerGallerySlider();
  galleryPopup();
  videoPopup();
  sidebarExpand();
  scrollNav();
  dropdownMenuHover();
  expandMenu();
  expandSearch();
  calProductGridHeight();
  filterExpand();
  productThumbClick();
  tabsClickScroll();
  productOtherScroll();
  faqClick();
  calNewsGridHeight();
  homeHotSlider();
  homeLogoSlider();
  cartContentScroll();
  popoverRun();
  calProductOtherHeight();
  videoContentScroll();
  tabsOverScroll();
  tooltipRun();
};
var initLoad = function(){
  //function run
};
var initResize = function(){
  //function run
  productOtherScroll();
};

//document ready before js
$(document).ready(function(){

  //js autoload when document ready
  initReady();

});

//window load all before js
$(window).load(function(){

  //js autoload when window load
  initLoad();

});

//window resize before js
$(window).resize(function() {

  //js autoload when window resize
  initResize();

});