//var window width
var viewportGlobal = $(window).width();

//function js sidebar Scroll
var sidebarScroll = function(){
  if($('.l-sidebar__inner').length > 0){
    $('.l-sidebar__inner').slimScroll({
      height: '100%'
    });
  }
};

//function js notify slider
var notifySlider = function(){
  if($('.c-notify').length > 0){
    $('#c-notify-id').owlCarousel({
      items:1,
      loop:true,
      margin:0,
      responsiveClass:false,
      nav:false,
      dots:false,
      autoplay:true,
      autoHeight:false,
      mouseDrag:false,
      touchDrag:false,
      animateOut: 'fadeOutUp',
      animateIn: 'fadeInUp',
      autoplayTimeout:6000,
      autoplaySpeed:1000,
      autoplayHoverPause:false,
      navText:false,
    });
  }
};

//function js main slider
var mainSlider = function(){
  if($('.c-slider').length > 0){
    $('#c-slider-id').owlCarousel({
      items:1,
      loop:true,
      margin:0,
      responsiveClass:false,
      nav:true,
      dots:true,
      autoplay:true,
      autoHeight:false,
      autoplayTimeout:6000,
      autoplaySpeed:1000,
      autoplayHoverPause:true,
      navText:false,
    });
  }
};

//function js client slider
var clientsSlider = function(){
  if($('.c-clients').length > 0){
    $('#c-clients-id').owlCarousel({
      loop:false,
      responsiveClass:true,
      nav:true,
      dots:false,
      autoplay:false,
      autoHeight:false,
      autoplayTimeout:6000,
      autoplayHoverPause:false,
      navText:false,
      responsive:{
        0:{
          margin:16,
          items:2
        },
        992:{
          margin:20,
          items:3
        },
        1200:{
          margin:40,
          items:3
        }
      },
      onInitialize: function (event) {
        if ($('.c-clients .owl-carousel .c-clients-item').length >3) {
          this.settings.loop = true;
          this.settings.autoplay = true;
        }
      }
    });
  }
};

//function js home banner slider
var homeBannerSlider = function(){
  if($('.c-home-banner-slider').length > 0){
    $('.js-c-home-banner-slider').owlCarousel({
      items:1,
      loop:true,
      margin:0,
      responsiveClass:false,
      nav:true,
      dots:false,
      autoplay:false,
      autoHeight:false,
      autoplayTimeout:8000,
      autoplaySpeed:1000,
      autoplayHoverPause:true,
      navText:false,
    });
  }
};

//function js home product slider
var homeProductSlider = function(){
  if($('.c-home-product-slider').length > 0){
    $('.js-c-home-product-slider').owlCarousel({
      loop:true,
      margin:0,
      responsiveClass:true,
      nav:true,
      dots:false,
      autoplay:false,
      autoHeight:false,
      autoplayTimeout:10000,
      autoplayHoverPause:false,
      navText:false,
      responsive:{
        0:{
          items:2
        },
        1600:{
          items:3
        }
      }
    });
  }
};

//function js map content scroll
var mapContentScroll = function(){
  if($('.js-c-map-scroll').length > 0){
    $('.js-c-map-scroll').slimScroll({
      height: '100%'
    });
  }
};

//function js map click
var mapContentClick = function(){
  if($('.c-map-item__content').length > 0){
    $(document).on('click','.c-map-item',function () {
      var src = $(this).data('map');
      $('.c-map-area__view iframe').attr('src',src);
      console.log(src);
    });
  }
};

//function js home promotion slider
var homePromotionSlider = function(){
  if($('.c-home-promotion-slider').length > 0){
    $('#c-home-promotion-id').owlCarousel({
      loop:true,
      center: true,
      margin:0,
      items:2,
      responsiveClass:true,
      nav:true,
      dots:false,
      autoplay:true,
      autoHeight:false,
      autoplayTimeout:8000,
      autoplayHoverPause:false,
      navText:false,
    });
  }
};

//function js footer gallery slider
var footerGallerySlider = function(){
  if($('.c-footer-gallery-slider').length > 0){
    $('#c-footer-gallery-id').owlCarousel({
      loop:true,
      center: true,
      margin:30,
      responsiveClass:true,
      nav:true,
      dots:false,
      autoplay:false,
      autoHeight:false,
      autoplayTimeout:10000,
      autoplayHoverPause:false,
      navText:false,
      responsive:{
        0:{
          items:3
        },
        1200:{
          items:5
        }
      }
    });
  }
};

//function js gallery popup
var galleryPopup = function(){
  $(".js-gallery").fancybox({
    padding: 10,
    openEffect	: 'none',
    closeEffect	: 'none',
  });
};

//function js video popup
var videoPopup = function(){
  $(".js-video").fancybox({
    width	: 930,
    height	: 535,
    padding: 10,
    aspectRatio : true,
    closeClick	: false,
    openEffect	: 'none',
    closeEffect	: 'none'
  });
};

//function js sidebar expand
var sidebarExpand = function(){
  if($('.l-sidebar').length > 0){
    $('.js-expand-sidebar').click(function(e) {
      e.preventDefault();
      var body = $('body');
      var menu = $('.l-sidebar');
      var page = $('.l-page');
      var ovelay = $('.c-app-ovelay');
      if (page.hasClass('has-page-open')) {
        page.removeClass('has-page-open');
        ovelay.removeClass('has-ovelay-show');
        menu.removeClass('has-menu-open');
        body.removeClass('has-body-open');
        $(this).removeClass('is-active');
      } else {
        page.addClass('has-page-open');
        ovelay.addClass('has-ovelay-show');
        menu.addClass('has-menu-open');
        body.addClass('has-body-open');
        $(this).addClass('is-active');
      }
    });
    $('.js-app-ovelay, .js-close').click(function() {
      var body = $('body');
      var menu = $('.l-sidebar');
      var page = $('.l-page');
      var ovelay = $('.c-app-ovelay');
      page.removeClass("has-page-open");
      ovelay.removeClass("has-ovelay-show");
      menu.removeClass("has-menu-open");
      body.removeClass("has-body-open");
      $('.js-expand-sidebar').removeClass('is-active');
      return false;
    });
  }
};

//function js scroll nav
var scrollNav = function(){
  if($('.l-header').length > 0){
    $(window).scroll(function(){
      var nav = $('.l-header');
      var mheight = nav.height();
      if($(this).scrollTop() > mheight){
        nav.addClass("has-fixed");
      }
      if($(this).scrollTop() < mheight){
        nav.removeClass("has-fixed");
      }
    });
  }
};

//function js dropdown menu hover
var dropdownMenuHover = function(){
  if($('.js-dropdown-menu-hover').length > 0){
    $('.js-dropdown-menu-hover').mouseenter(function () {
      $(this).addClass('is-active');
      if($(this).data('bg') === true){
        $('.l-page').addClass('has-bg');
      }
    }).mouseleave(function () {
      $(this).removeClass('is-active');
      if($(this).data('bg') === true){
        $('.l-page').removeClass('has-bg');
      }
    });
  }
};

//function js expand menu
var expandMenu = function(){
  if($('.c-menu__child').length > 0){
    $('.c-menu__child a').click(function(e) {
      e.preventDefault();
      var menu = $('.c-menu');
      var menuChild = $(this).parent();
      if(menuChild.hasClass('is-active')){
        $('.c-menu__expand', $(this)).removeClass('fa-angle-up').addClass('fa-angle-down');
        menuChild.removeClass('is-active');
        $('.c-menu-sub',menuChild).slideUp();
      }else {
        $('ul >li.is-active >a >.c-menu__expand',menu).removeClass('fa-angle-up').addClass('fa-angle-down');
        $('ul >li.is-active >.c-menu-sub',menu).slideUp();
        $('ul >li.is-active',menu).removeClass('is-active');
        $('.c-menu__expand', $(this)).removeClass('fa-angle-down').addClass('fa-angle-up');
        menuChild.addClass('is-active');
        $('.c-menu-sub',menuChild).slideDown();
      }
    });
  }
};

//function js expand search
var expandSearch = function(){
  if($('.js-expand-search').length > 0){
    $('.js-expand-search').click(function(e) {
      e.preventDefault();
      var boxSearch = $('.c-search');
      if (boxSearch.hasClass('is-active')) {
        $(this).removeClass('is-active');
        $('.fa',$(this)).removeClass('fa-times').addClass('fa-search');
        boxSearch.removeClass('is-active')
            .hide();
      } else {
        $(this).addClass('is-active');
        $('.fa',$(this)).removeClass('fa-search').addClass('fa-times');
        boxSearch.addClass('is-active')
            .show();
      }
    });
  }
};

//function js cal Product grid height
var calProductGridHeight = function(){
  if($('.product-grid').length > 0){
    $('.product-grid >ul >li').matchHeight();
  }
};

//function js filter expand
var filterExpand = function(){
  if($('.browse-fillter').length > 0){
    $('.browse-fillter__title').click(function() {
      var parent = $(this).parent();
      if(parent.hasClass('is-active')){
        parent.removeClass('is-active');
        $('.browse-fillter__content',parent)
        $('.browse-fillter__btn',$(this)).removeClass('fa-angle-up').addClass('fa-angle-down');
        $('.browse-fillter__content',parent).slideUp();
      }else {
        parent.addClass('is-active');
        $('.browse-fillter__btn',$(this)).removeClass('fa-angle-down').addClass('fa-angle-up');
        $('.browse-fillter__content',parent).slideDown();
      }
    });
  }
};

//function js cal Product thumb click
var productThumbClick = function(){
  if($('.c-detail-img__thumb').length > 0){
    $('.c-detail-img__thumb ul li a').click(function(e) {
      e.preventDefault();
      var href = $(this).attr('href');
      $('.c-detail-img__main img').attr('src',href);
    });
  }
};

//function js tabs click scroll
var tabsClickScroll = function(){
  if($('.c-tabs').length > 0){
    $('.c-tabs .c-tabs__title ul li a').click(function(e) {
      e.preventDefault();
      var id = $(this).attr('href');
      $("html, body").animate({scrollTop: $(id).offset().top - 65 - 54}, 700);
    });
  }
};

//function js product other scroll
var productOtherScroll = function(){
  if($('.js-product-float').length > 0){
    $(window).scroll(function(){
      var nav = $('.js-product-float');
      var navHeight =  nav.height();
      var navParent = nav.parent();
      var navParentHeight = navParent.height();
      var navGrandParent = nav.parent().parent();
      var navGrandParentHeight =  navGrandParent.height();
      var navGrandParentTop = navGrandParent.offset().top;
      var beforeHeight = 0;
      if ($('.js-before-float').length > 0){
        beforeHeight = $('.js-before-float').height();
        navGrandParentTop = navGrandParentTop + beforeHeight;
      }
      var navBottom = navGrandParentTop + navGrandParentHeight;
      var navParentWidth = navParent.width();
      var keepBottomTop = navGrandParentTop + (navGrandParentHeight - navHeight - beforeHeight);

      //add class: js-content-over-float --> div main content left
      if ($('.js-content-over-float').length > 0 && $('.js-content-over-float').height() >= navParentHeight){
        if ($(this).scrollTop() > (navGrandParentTop - 65) && $(this).scrollTop() < (navBottom - 65)) {
          nav.addClass('has-fixed');
          nav.css({'width':navParentWidth});
        } else {
          nav.removeClass('has-fixed');
          nav.css({'width':'auto'});
        }
        if ($(this).scrollTop() > (keepBottomTop - 65) && $(this).scrollTop() < (navBottom - 65)) {
          nav.addClass('has-ab');
        } else {
          nav.removeClass('has-ab');
        }
      }
    });
  }
};

//function js faq click
var faqClick = function(){
  if($('.c-faq-item').length > 0){
    $(document).on('click','.c-faq-item .c-faq-item__title',function(){
      var faqList = $('.c-faq-list');
      var parent = $(this).parent();
      if(parent.hasClass('is-active')){
        parent.removeClass('is-active');
        $('.c-faq-item__content',parent).slideUp();
        $('.c-faq-item__caret',$(this)).removeClass('fa-angle-up').addClass('fa-angle-down');
      }
      else{
        $('.c-faq-item.is-active .c-faq-item__content',faqList).slideUp();
        $('.c-faq-item.is-active .c-faq-item__title .c-faq-item__caret',faqList).removeClass('fa-angle-up').addClass('fa-angle-down');
        $('.c-faq-item.is-active',faqList).removeClass('is-active');
        parent.addClass('is-active');
        $('.c-faq-item__content',parent).slideDown();
        $('.c-faq-item__caret',$(this)).removeClass('fa-angle-down').addClass('fa-angle-up');
      }
    });
  }
};

//function js cal news grid height
var calNewsGridHeight = function(){
  if($('.c-news-grid').length > 0){
    $('.c-news-grid >ul >li').matchHeight();
  }
};

//var function home hot slider
var homeHotSlider = function(){
  if($('.c-home-hot-slider').length > 0){
    var homeHotSwiper = new Swiper('.c-home-hot-slider .swiper-container', {
      paginationClickable: true,
      direction: 'vertical',
      slidesPerView: 4,
      loop: true,
      autoplay: 6000,
      autoplayDisableOnInteraction: false
    });
  }
};

//var function home logo slider
var homeLogoSlider = function(){
  if($('.c-home-logo-slider').length > 0){
    var homeLogoSwiper = new Swiper('.c-home-logo-slider .swiper-container', {
      paginationClickable: true,
      direction: 'vertical',
      slidesPerView: 4,
      loop: true,
      autoplay: 6000,
      autoplayDisableOnInteraction: false
    });
  }
};

//function js cart content scroll
var cartContentScroll = function(){
  if($('.c-payments__right').length > 0){
    $('.js-cart-product-scroll').slimScroll({
      height: '100%'
    });
  }
};

//function js popover
var popoverRun = function(){
  $('[data-toggle="popover"]').popover();
};

//function js cal Product Other height
var calProductOtherHeight = function(){
  if($('.c-product-float').length > 0){
    $('.c-product-float ul >li').matchHeight();
  }
};

//function js video content scroll
var videoContentScroll = function(){
  if($('.c-home-video').length > 0){
    $('.js-home-video').slimScroll({
      height: '100%'
    });
  }
};

//function js tabs over scroll
var tabsOverScroll = function(){
  if($('.js-tabs-over-float').length > 0){
    $(window).scroll(function(){
      var nav = $('.js-tabs-over-float');
      var navHeight =  nav.height();
      var navParent = nav.parent();
      var navParentHeight = navParent.height();
      var navParentTop = navParent.offset().top;
      var navParentWidth = navParent.width();
      var navBottom = navParentTop + navParentHeight;
      if ($(this).scrollTop() > (navParentTop - 65) && $(this).scrollTop() < (navBottom - 65)) {
        nav.addClass('has-fixed');
        $('.c-tabs__title', nav).css({'width':navParentWidth});
      } else {
        nav.removeClass('has-fixed');
        $('.c-tabs__title', nav).css({'width':'auto'});
      }
    });
  }
};

//function js tooltip run
var tooltipRun = function(){
  $('[data-toggle="tooltip"]').tooltip();
};